<?php 

class Usuario {

    private $idusuario;
    private $deslogin;
    private $dessenha;
    private $dtcadastro;

    public function getIdusuario(){
        return $this->idusuario;
    }

    public function setIdusuario($idusuario){
        $this->idusuario = $idusuario;
    }

    public function getDeslogin(){
        return $this->deslogin;
    }

    public function setDeslogin($deslogin){
        $this->deslogin = $deslogin;
    }

    public function getDessenha(){
        return $this->dessenha;
    }

    public function setDessenha($dessenha){
        $this->dessenha = $dessenha;
    }

    public function getDtcadastro(){
        return $this->dtcadastro;
    }

    public function setDtcadastro($dtcadastro){
        $this->dtcadastro = $dtcadastro;
    }

    public function loadById($id){
        $sql = new Sql;

        $results = $sql->select("SELECT * FROM tb_usuarios where idusuario = :ID",["ID" =>$id]);

        if(isset($results)){

            $this->setData($results[0]);

        }
    }

    public static function getList(){

        $sql = new Sql();

        return $sql->select("SELECT * FROM tb_usuarios");
    }

    public static function search($login){

        $sql = new Sql();

        return $sql->select("SELECT * FROM tb_usuarios WHERE deslogin LIKE :SEARCH ",[':SEARCH' =>"%".$login."%"]);

    }

    public function login($login,$senha){
        $sql = new Sql;

        $results = $sql->select("SELECT * FROM tb_usuarios WHERE deslogin = :LOGIN AND dessenha = :SENHA",[":LOGIN" =>$login,":SENHA"=>$senha]);

        if(count($results) > 0){

            $this->setData($results[0]);
            
        } else {
            throw new Exception("Login ou senha invalidos");
        }
    }

    public function setData($data){

        $this->setIdusuario($data['idusuario']);
        $this->setDeslogin($data['deslogin']);
        $this->setDessenha($data['dessenha']);
        $this->setDtcadastro($data['dtcadastro']);

    }

    public function insert(){

        $sql = new Sql;

        $results = $sql->select("CALL sp_usuarios_insert(:LOGIN,:PASSWORD)",['LOGIN' => $this->getDeslogin(), 'PASSWORD' => $this->getDessenha()]);

        if(count($results) > 0){

            $this->setData($results[0]);
            
        }
    }

    public function update($login,$senha){

        $this->setDeslogin($login);
        $this->setDessenha($senha);

        $sql = new Sql();

        $sql->query("UPDATE tb_usuarios SET deslogin = :LOGIN, dessenha = :SENHA WHERE idusuario = :ID",[
        ':LOGIN' => $this->getDeslogin(),
        ':SENHA' => $this->getDessenha(),
        ':ID' => $this->getIdusuario()
        ]);
    }

    public function delete(){

        $sql = new Sql();

        $sql->query("DELETE FROM tb_usuarios WHERE idusuario = :ID",[
            ':ID' => $this->getIdusuario()
            ]);

        $this->setIdusuario(null);
        $this->setDeslogin(null);
        $this->setDessenha(null);
        $this->setDtcadastro(null);
    }

    public function __construct($login = "", $senha = ""){
        $this->deslogin = $login;
        $this->dessenha = $senha;
    }

    public function __toString(){
        return json_encode([
            "idusuario" => $this->getIdusuario(),
            "deslogin" => $this->getDeslogin(),
            "dessenha" => $this->getDessenha(),
            "dtcadastro" => $this->getDtcadastro(),
        ]);
    }

}