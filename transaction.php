<?php



$conn = new PDO("mysql:host=localhost;dbname=dbphp7","root", "");

$conn->beginTransaction(); #Inicia uma nova transação

$stmt = $conn->prepare("DELETE FROM tb_usuarios WHERE idusuario = ?");
 
$id = 8;

$stmt->execute([$id]);

#Só funciona se a engine for InnoDB
//$conn->rollBack();  Cancela uma transação 

$conn->commit(); #Confirma a transação

echo "Deletado OK"; 